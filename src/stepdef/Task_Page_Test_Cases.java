package stepdef;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import basepack.Test_Setup_Class;
import pageobjectspack.My_Tasks_Page;
import pageobjectspack.Login_Page;
import pageobjectspack.Home_Page;

/* Class defines the test cases to be executed on the My Tasks
 * page of the Todo app
 */
public class Task_Page_Test_Cases extends Test_Setup_Class{
	
	public static WebDriver driver;
	
	//Objects for the page classes
	public static Login_Page lp;
	public static Home_Page hp;
	public static My_Tasks_Page mt;
	
	@Given("^User on the My Tasks page$")
	public void user_on_the_my_tasks_page() throws Throwable{
		driver = setDriver();
		lp = new Login_Page(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Perform login
		lp.performSignIn();
	}
	
	@When("^User clicks on My Tasks link$")
	public void user_navigates_to_my_tasks_page() throws Throwable{
		hp = new Home_Page(driver);
		
		//Click on My Tasks link
		hp.clickMyTasks();
	}
	
	@Then("^Message displayed Hey Promila this is your todo list for today:$")
	public void message_displayed_hey_promila_this_is_your_todo_list_for_today() throws Throwable{
		mt = new My_Tasks_Page(driver);
		//Verify if the correct text message is displayed for the user.
		Assert.assertTrue("Incorrect user message is displayed on My Tasks page",mt.verifyMyTasksMessage());
		driver.quit();
	}

}
