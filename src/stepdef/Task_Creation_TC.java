package stepdef;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import basepack.Test_Setup_Class;
import pageobjectspack.My_Tasks_Page;
import pageobjectspack.Login_Page;
import pageobjectspack.Home_Page;



public class Task_Creation_TC extends Test_Setup_Class{
	
	public static WebDriver driver;
	public static Login_Page lp;
	public static Home_Page hp;
	public static My_Tasks_Page mt;
	
	@Given("^User is on the My Tasks$")
	public void user_on_the_my_tasks() throws Throwable{
		driver = setDriver();
		lp = new Login_Page(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		lp.performSignIn();
		hp = new Home_Page(driver);
		//Click on My Tasks link
		hp.clickMyTasks();
	}
	
	@When("^User enters description of task as write email$")
	public void user_enters_description_of_task_as() throws Throwable{
		
		//Enter task description as "write email"
		mt.enterTask("write email");
	}
	
	@And("^User presses enter from keyboard$")
	public void user_presses_enter_from_keyword() throws Throwable{
		
		//Press enter key
		mt.pressEnter();
	}
	
	@Then("^Task is displayed in Todo list below$")
	public void task_is_displayed_in_todo_list_below() throws Throwable{
		
		//Verify if the task is added in the list
		//task should be present in the Todo list
		Assert.assertTrue("Task is not added in the list",mt.verifyTaskPresent("write email"));
		driver.quit();
	}
	
}
