package stepdef;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import basepack.Test_Setup_Class;
import pageobjectspack.My_Tasks_Page;
import pageobjectspack.Login_Page;
import pageobjectspack.Home_Page;

/* Class defines the test cases to be executed on the home
 * page of the Todo app
 */
public class Home_Page_Test_Cases extends Test_Setup_Class{
	
	public static WebDriver driver;
	
	//Objects of the page classes
	public static Login_Page lp;
	public static Home_Page hp;
	public static My_Tasks_Page mt;

	@Given("^User is on Home page$")
	public void user_is_on_home_page() throws Throwable{
		driver = setDriver();
		lp = new Login_Page(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Perform login
		lp.performSignIn();
	}
	
	@Then("My Tasks link is present on home page")
	public void my_tasks_link_is_present_on_home_page() throws Throwable{
		hp = new Home_Page(driver);
		
		//Asserts that the My tasks link is present on the home page
		Assert.assertTrue("My Tasks link is not present on home page",hp.verifyMyTasksLink());
		driver.quit();
	}

}
