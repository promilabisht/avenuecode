package stepdef;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import basepack.Test_Setup_Class;
import pageobjectspack.My_Tasks_Page;
import pageobjectspack.Login_Page;
import pageobjectspack.Home_Page;
import pageobjectspack.Subtask_Page;

/* Class defines the test cases to be executed on the Subtask modal
 * page of the Todo app
 */
public class Subtask_Creation_TC extends Test_Setup_Class{
	
	public static WebDriver driver;
	
	//Objects of the page classes
	public static Login_Page lp;
	public static Home_Page hp;
	public static My_Tasks_Page mt;
	public static Subtask_Page sp;
	
	@Given("^User is on Tasks page$")
	public void user_on_tasks_page() throws Throwable{
		driver = setDriver();
		lp = new Login_Page(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Perform login
		lp.performSignIn();
		hp = new Home_Page(driver);
		
		//Click on My Tasks link
		hp.clickMyTasks();
		mt = new My_Tasks_Page(driver);
		
		//Asserts if the Manage subtasks button is present on the page
		Assert.assertTrue("Manage subtask button is not present",mt.verifyManageSubtaskBtnPresent());
	}
	
	@When("^User clicks Manage Subtasks button$")
	public void user_clicks_manage_subtasks_button() throws Throwable{
		
		//Click on the Manage Subtasks button
		mt.clickManageSubtask();
		sp = new Subtask_Page(driver);
		
		//Check if the Task id is present in the subtask modal dialog
		Assert.assertTrue("Task id is not displayed",sp.verifyTaskId());
	}
	
	@And("^User enters Subtask description$")
	public void user_presses_enter_from_keyword() throws Throwable{
		
		//Enter sub task description as "send email"
		sp.enterSubTask("send email");
	}
	
	@And("^User clicks button add$")
	public void user_clicks_button_add() throws Throwable{
		
		//Click add button on subtask dialog
		sp.clickAdd();
	}
	
	@Then("^SubTask is displayed in Subtask Todo list$")
	public void subtask_is_displayed_in_subtask_todo_list() throws Throwable{
		
		//Checks if the subtask is present in the subtask list or not
		Assert.assertTrue("SubTask is not added in the list",sp.verifySubTaskPresent("send email"));
		driver.quit();
	}
	
	
}
