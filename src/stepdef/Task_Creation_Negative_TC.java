package stepdef;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import basepack.Test_Setup_Class;
import pageobjectspack.My_Tasks_Page;
import pageobjectspack.Login_Page;
import pageobjectspack.Home_Page;

/* Class defines the negative test cases to be executed on the My Tasks
 * page of the Todo app
 */
public class Task_Creation_Negative_TC extends Test_Setup_Class{
	
	public static WebDriver driver;
	
	//Objects of the page classes
	public static Login_Page lp;
	public static Home_Page hp;
	public static My_Tasks_Page mt;
	
	@Given("^User is on My Tasks page$")
	public void user_on_my_tasks_page() throws Throwable{
		driver = setDriver();
		lp = new Login_Page(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Perform login
		lp.performSignIn();
		hp = new Home_Page(driver);
		
		//Click on My Tasks link
		hp.clickMyTasks();
	}
	
	@When("^User enters description of task as ab$")
	public void user_enters_description_of_task_as_ab() throws Throwable{
		
		//Enter task description with less than 3 characters
		//Invalid entry for negative test case
		mt.enterTask("ab");
	}
	
	@And("^User presses enter next$")
	public void user_presses_enter_next() throws Throwable{
		
		//Press enter key
		mt.pressEnter();
	}
	
	@Then("^Task is not displayed in Todo list below$")
	public void task_is_not_displayed_in_todo_list_below() throws Throwable{
		
		/*Verify if the task is not added.
		 * As it is negative test case, task with the given
		 * description should not be added in the task list.
		 */
		Assert.assertFalse("Task is not added in the list",mt.verifyTaskPresent("ab"));
		driver.quit();
	}
	
}
