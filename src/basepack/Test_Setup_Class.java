package basepack;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/*Class defined to perform test setup before test execution
 * Return value: WebDriver object
 */
public class Test_Setup_Class {

	public static WebDriver setDriver() {
			
			//Create driver for firefox
			WebDriver driver = new FirefoxDriver();
			
			//maximize the window
			driver.manage().window().maximize();
			
			//Open the app url
			driver.navigate().to("http://qa-test.avenuecode.com/users/sign_in");
			return driver;
	}
	
}