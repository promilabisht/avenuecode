package pageobjectspack;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/*Class defined for the Subtask page
 * It defines all actions/verifications to be performed
 * on this page.
 */
public class Subtask_Page {

private WebDriver driver;

//Variables defined for web elements on the page
private By subtaskTextBox = By.id("new_sub_task");
private By addBtn = By.id("add-subtask");
private By taskidTxt = By.xpath("html/body/div[4]/div/div/div[1]/h3");
	
//Constructor
	public Subtask_Page(WebDriver driver) {
		this.driver=driver;
	}
	
	//Enter Sub task description in the textbox
	public void enterSubTask(String subtaskDesc) {
		//Locate the subtask description textbox
		WebElement mytaskTbox = driver.findElement(subtaskTextBox);
		
		//Check if the textbox is displayed on the page
		if(mytaskTbox.isDisplayed())
			
			//enter description in the textbox
			mytaskTbox.sendKeys(subtaskDesc);
	}
	
	//Click add button
	public void clickAdd() {
		
		//Locate the add button on the dialog box
		WebElement addsubtaskBtn = driver.findElement(addBtn);
		
		//Check if the button is displayed
		if(addsubtaskBtn.isDisplayed())
			
			//Click on the button
			addsubtaskBtn.click();
	}
	
	//Verify if the sub task is present in the list after adding it
	public Boolean verifySubTaskPresent(String subtaskDesc){
		
		//Locate the sub task in the list with the text as subtask description
		List<WebElement> txtlist = driver.findElements(By.xpath("//*[contains(text(),'" + subtaskDesc + "')]"));
		
		//Check if the list returned above is empty or not
		if (txtlist.size() > 0)
			return true;
		else
			return false;
	}
	
	//Verify if the task id is present on the page
	public Boolean verifyTaskId(){
		
		//Locate task id element in the page
		WebElement idTxt = driver.findElement(taskidTxt);
		
		//Check if the element is displayed
		if(idTxt.isDisplayed())
			return true;
		else
			return false;
	}
		
}