package pageobjectspack;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/*Class defined for the My Tasks page
 * in the Todo app.
 * It defines all actions/verifications to be performed on the page.
 * 
 */
public class My_Tasks_Page {

private WebDriver driver;

//Variables defined for the web elements present on the page
private By userMessage = By.xpath("html/body/div[1]/h1");
private By taskTextBox = By.id("new_task");
private By addBtn = By.xpath("html/body/div[1]/div[2]/div[1]/form/div[2]/span");

private String msgText = "Hey Promila, this is your todo list for today:";
	
	//Constructor
	public My_Tasks_Page(WebDriver driver) {
		this.driver=driver;
	}
	
	//Create new task using Add button
	public void createTaskUsingBtn(String taskDesc) {
		
		//Call function to enter task description
		enterTask(taskDesc);
		
		//Call function to click on the Add button
		clickAdd();
	}
	
	//Create new task after pressing enter key
	public void createTaskPressEnter(String taskDesc) {
		
		//Call function to enter task description
		enterTask(taskDesc);
		
		//Call function to press enter key
		pressEnter();
	}
	
	//Enter task description in textbox
	public void enterTask(String taskDesc) {
		
		//Locate task description textbox
		WebElement mytaskTbox = driver.findElement(taskTextBox);
		
		//Check if the text box is present on the page
		if(mytaskTbox.isDisplayed())
			
			//Enter task description in the textbox
			mytaskTbox.sendKeys(taskDesc);
	}
	
	
	//Click on the add button
	public void clickAdd() {
		
		//Locate the add button on page
		WebElement addtaskBtn = driver.findElement(addBtn);
		
		//Check if the button is present
		if(addtaskBtn.isDisplayed())
			
			//Click on the button
			addtaskBtn.click();
	}
	
	//Click on the Manage subtask button
	public void clickManageSubtask() {
		
		//Locate the manage subtasks button with text as Manage Subtasks
		List<WebElement> subtaskBtn = driver.findElements(By.xpath("//*[contains(text(),'Manage Subtasks')]"));
		
		//Check if the list of elements returned is empty or not
		if(subtaskBtn.size()>0)
			
			//List is not empty so click on one of the buttons available
			subtaskBtn.get(1).click();
	}
	
	//Press enter key
	public void pressEnter() {
		
		//Locate the task text box on the page
		WebElement mytaskTbox = driver.findElement(taskTextBox);
		
		//enter task description in the textbox
		mytaskTbox.sendKeys(Keys.RETURN);
	}
	
	//Verify if the message for the user on My Tasks page is correct
	public Boolean verifyMyTasksMessage(){
		
		//Get the text on the message displayed on page
		String userTxt = driver.findElement(userMessage).getText();
		
		//If the text contains the valid message return true else false
		if (userTxt.contains(msgText))
			return true;	
		else
			return false;
	}
	
	//Verify if the task is present in the todo list or not
	public Boolean verifyTaskPresent(String taskDesc){
		
		//Locate the task in the list with text as the task description
		List<WebElement> txtlist = driver.findElements(By.xpath("//*[contains(text(),'" + taskDesc + "')]"));
		
		//Check if the list of elements returned above is empty or not
		if (txtlist.size() > 0)
			return true;
		else
			return false;
	}
	
	//Verify if the Manage subtask button  is present on teh page or not
	public Boolean verifyManageSubtaskBtnPresent(){
		
		//Locate the button with text Manage Subtasks
		List<WebElement> btnlist = driver.findElements(By.xpath("//*[contains(text(),'Manage Subtasks')]"));	
		
		//Check if the list returned above is empty or notß
		if (btnlist.size() > 0)
			return true;
		else
			return false;
	}
		
}