package pageobjectspack;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/*Class defined for the home page of ToDo app
 * It defines the actions/validations to be performed
 * on the home page
 */
public class Home_Page {

private WebDriver driver;

private By tasklink = By.xpath("html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a");
	
	//Constructor
	public Home_Page(WebDriver driver) {
		this.driver=driver;
	}
	
	/*Verify whether My Tasks link is
	 * present on the home page
	 */
	public Boolean verifyMyTasksLink(){
		Boolean isPresent = driver.findElements(tasklink).size() > 0;
		return isPresent;	
	}
	
	/*Click the My Tasks link on home page
	 * of app
	 */
	public void clickMyTasks(){
		
		//find the My tasks link
		WebElement mytaskElt = driver.findElement(tasklink);
		
		//Check if the link is present
		if(mytaskElt.isDisplayed())
			
			//Click on the link
			mytaskElt.click();
	}
		
}