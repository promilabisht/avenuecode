package pageobjectspack;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/*Class defined for the Login page of the 
 * Todo app.
 * It defines all actions/verifications to be 
 * performed on the login page.
 */
public class Login_Page {

private WebDriver driver;

//Variables for web elements on the page
private By emailTextBox = By.id("user_email");
private By passwordTextBox = By.id("user_password");
private By signinBtn = By.xpath(".//*[@id='new_user']/input");

	//Constructor
	public Login_Page(WebDriver driver) {
		this.driver=driver;
	}
	
	//Perform login
	public void performSignIn() {
		
		//Enter email in textbox
		enterEmail("promila.bisht5@gmail.com");
		
		//enter password in textbox
		enterPassword("promilabisht");
		
		//Click on the Sign in button
		clickSignIn();
	}
	
	//Enter email in the textbox
	public void enterEmail(String userName) {
		
		//Locate the email textbox
		WebElement emailTxtBox = driver.findElement(emailTextBox);
		
		//Check if the element is present
		if(emailTxtBox.isDisplayed())
			
			//Enter email in the text box
			emailTxtBox.sendKeys(userName);
	}
	
	//Enter password in the text box
	public void enterPassword(String password) {
		
		//Locate the password textbox
		WebElement passwordTxtBox = driver.findElement(passwordTextBox);
		
		//Check if the textbox for password is present
		if(passwordTxtBox.isDisplayed())
			
			//Enter the password
			passwordTxtBox.sendKeys(password);
	}
	
	//Click on the Sign in button
	public void clickSignIn() {
		
		//Locate the Sign in button
		WebElement signInBtn = driver.findElement(signinBtn);
		
		//Check if the button is present
		if(signInBtn.isDisplayed())
			
			//Click on the Sign in button
			signInBtn.click();
	}
		
}