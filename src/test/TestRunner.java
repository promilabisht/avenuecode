package test;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "Feature"
		,glue = {"stepdef"}
		)

/* Main class that runs the test framework
 * It si the entry point.
 */
public class TestRunner {
	
}
