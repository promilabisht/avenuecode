Feature: Task creation functionality

@taskcreation
Scenario: Successful creation of task
Given User is on the My Tasks
When User enters description of task as write email
And User presses enter from keyboard
Then Task is displayed in Todo list below