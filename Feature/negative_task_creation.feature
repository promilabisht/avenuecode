Feature: Task creation

@negativetaskcreation
Scenario: Should not create task having description less than 3 chars
Given User is on My Tasks page
When User enters description of task as ab
And User presses enter next
Then Task is not displayed in Todo list below